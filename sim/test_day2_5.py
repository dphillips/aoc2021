import cocotb
from cocotb.triggers import FallingEdge
from cocotb.clock import Clock

@cocotb.test()
async def test_day2(dut):
    clock = Clock(dut.clk_i, 1, 'ns')
    cocotb.start_soon(clock.start())

    dut.rst_i.value = 1
    await FallingEdge(dut.clk_i)
    await FallingEdge(dut.clk_i)
    dut.rst_i.value = 0

    for i in range(6):
        await FallingEdge(dut.clk_i)
        assert dut.answer_ready_o.value == 0

    await FallingEdge(dut.clk_i)
    assert dut.answer_ready_o.value == 1
    assert dut.answer_o.value == 900
