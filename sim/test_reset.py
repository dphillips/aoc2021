import cocotb
from cocotb.triggers import FallingEdge
from cocotb.clock import Clock

@cocotb.test()
async def test_reset(dut):
    clock = Clock(dut.clk_i, 1, 'ns')
    cocotb.start_soon(clock.start())

    for i in range(7):
        await FallingEdge(dut.clk_i)
        assert dut.rst_o.value == 1

    await FallingEdge(dut.clk_i)
    assert dut.rst_o.value == 0


