#!/usr/bin/awk -f
{
switch ($1) {
	case "forward":
		command_tok = 0;
		break;
	case "up":
		command_tok = 1;
		break;
	case "down":
		command_tok = 2;
		break;
}

printf("%01x%03x\n", command_tok, $2)
}
