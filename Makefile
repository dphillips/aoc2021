TOP = top
OBJS = top.v hdl/reset_gen.v $(shell find hdl -name "day*.v")
SIM ?= icarus

FREQ_MHZ = 25
DEVICE_TYPE = 25k
PACKAGE = CABGA256
SPEED_GRADE = 6

.PHONY: all input clean program lint sim
all: lint $(TOP).svf

input:
	make -C input all

clean:
	rm -f *.svf *.bit *.json

lint:
	iverilog -g2005-sv -tnull $(OBJS)

sim: input
	$(MAKE) -C sim VERILOG_SOURCES="$(addprefix ../, $(OBJS))" TOPLEVEL=reset_gen all clean
	$(MAKE) -C sim VERILOG_SOURCES="$(addprefix ../, $(OBJS))" TOPLEVEL=day1 all clean
	$(MAKE) -C sim VERILOG_SOURCES="$(addprefix ../, $(OBJS))" TOPLEVEL=day2 all clean
	$(MAKE) -C sim VERILOG_SOURCES="$(addprefix ../, $(OBJS))" TOPLEVEL=day2_5 all clean
	$(MAKE) -C sim VERILOG_SOURCES="$(addprefix ../, $(OBJS))" TOPLEVEL=day3 all clean
	$(MAKE) -C sim VERILOG_SOURCES="$(addprefix ../, $(OBJS))" TOPLEVEL=day3_5 all clean

program: $(TOP).svf
	# probably not very useful to anyone without more configurable paths
	openocd -f /usr/share/openocd/scripts/interface/ftdi/dp_busblaster_kt-link.cfg \
	        -f ~/jtag/5a-75e.cfg \
	        -c "init; svf $^; shutdown"

%.json: $(OBJS)
	yosys -f "verilog -sv" -p "synth_ecp5 -json $@" $(OBJS)

%.config: %.json %.lpf
	nextpnr-ecp5 \
		--$(DEVICE_TYPE) \
		--package $(PACKAGE) \
		--speed $(SPEED_GRADE) \
		--freq $(FREQ_MHZ) \
		--json $< \
		--textcfg $@ \
		--lpf $(word 2,$^)

%.bit %.svf: %.config
	ecppack \
		--bit "$*.bit" \
		--svf "$*.svf" \
		--input "$^"
