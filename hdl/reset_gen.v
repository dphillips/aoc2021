module reset_gen #(
	parameter RESET_DURATION_CLOCKS = 8
)(
	input clk_i,
	output reg rst_o
);

reg[$clog2(RESET_DURATION_CLOCKS):1] counter = 0;

initial begin
	counter <= 0;
	rst_o <= 1'b1;
end

always @(posedge clk_i) begin
	counter <= counter + 1;
	if (counter == RESET_DURATION_CLOCKS - 1) begin
		rst_o <= 1'b0;
	end
end

endmodule
