module day2 #(
	parameter ITEM_COUNT = 6,
	parameter COMMAND_WIDTH = 4,
	parameter VALUE_WIDTH = 12
)(
	input clk_i,
	input rst_i,
	output reg [31:0]answer_o,
	output answer_ready_o
);

reg [COMMAND_WIDTH+VALUE_WIDTH-1:0]data_array[ITEM_COUNT-1:0];

reg [$clog2(ITEM_COUNT):0]i;

parameter s_busy = 0, s_done = 1;
reg state;

assign answer_ready_o = state == s_done;

initial begin
	$readmemh("../input/day2.hex", data_array);
end

parameter cmd_forward=0, cmd_up=1, cmd_down=2;
wire [COMMAND_WIDTH+VALUE_WIDTH-1:0] command = data_array[i][COMMAND_WIDTH+VALUE_WIDTH-1:VALUE_WIDTH];
wire [VALUE_WIDTH-1:0]value = data_array[i][VALUE_WIDTH-1:0];

reg [31:0]horizontal;
reg [31:0]depth;

always @(posedge clk_i) begin
	if (rst_i == 1'b1) begin
		i <= 0;
		answer_o <= 0;
		state <= s_busy;
		horizontal <= 0;
		depth <= 0;
	end else begin
		case (state)
			s_busy: begin
				i <= i + 1;
				if (i == ITEM_COUNT) begin
					answer_o <= depth * horizontal;
					state <= s_done;
				end else begin
					case (command)
						cmd_forward: horizontal <= horizontal + value;
						cmd_up: depth <= depth - value;
						cmd_down: depth <= depth + value;
						default: ;
					endcase
				end
			end
		endcase
	end
end

`ifdef COCOTB_SIM
initial begin
	$dumpfile("day2.vcd");
	$dumpvars(0, day2);
end
`endif
endmodule
