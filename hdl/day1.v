module day1 #(
	parameter ITEM_COUNT = 2000,
	parameter ITEM_WIDTH = 32,
	parameter WINDOW_SIZE = 1
)(
	input clk_i,
	input rst_i,
	output reg [$clog2(ITEM_COUNT):0]answer_o,
	output answer_ready_o
);

reg [ITEM_WIDTH-1:0]data_array[ITEM_COUNT-1:0];

reg [$clog2(ITEM_COUNT):0]i;

parameter s_busy = 0, s_done = 1;
reg state;

reg [$clog2(ITEM_COUNT):0] answer;
assign answer_ready_o = state == s_done;

initial begin
	$readmemh("../input/day1.hex", data_array);
end


integer j;

reg [ITEM_WIDTH-1:0]sum_a;
reg [ITEM_WIDTH-1:0]sum_b;

always @(posedge clk_i) begin
	if (rst_i == 1'b1) begin
		i <= 0;
		answer_o <= 0;
		answer <= 0;
		state <= s_busy;
	end else begin
		case (state)
			s_busy: begin
				i <= i + 1;
				if (i == ITEM_COUNT - WINDOW_SIZE) begin
					answer_o <= answer;
					state <= s_done;
				end else begin
					sum_a = 0;
					for (j = 0; j < WINDOW_SIZE; j++)
						sum_a = sum_a + data_array[i+j];
					sum_b = 0;
					for (j = 1; j < WINDOW_SIZE+1; j++)
						sum_b = sum_b + data_array[i+j];
					if (sum_a < sum_b)
						answer <= answer + 1;
				end
			end
		endcase
	end
end

`ifdef COCOTB_SIM
initial begin
	$dumpfile("day1.vcd");
	$dumpvars(0, day1);
end
`endif
endmodule
