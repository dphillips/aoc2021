module day3 #(
	parameter ITEM_COUNT = 12,
	parameter ITEM_WIDTH = 5
)(
	input clk_i,
	input rst_i,
	output reg [31:0] answer_o,
	output reg answer_ready_o
);

reg [ITEM_WIDTH-1:0]data_array[ITEM_COUNT-1:0];
reg [ITEM_WIDTH-1:0]gamma;
reg [$clog2(ITEM_COUNT)-1:0]item_num;

reg [$clog2(ITEM_COUNT)-1:0]bit_sum[ITEM_WIDTH-1:0];
integer bitnum;

parameter s_busy = 0, s_done = 1;
reg state;


initial begin
	$readmemb("../input/day3.bin", data_array);
end

always @(posedge clk_i) begin
	if (rst_i == 1'b1) begin
		item_num <= 0;
		state <= s_busy;
		answer_o <= 0;
		answer_ready_o <= 0;
		gamma <= 0;
		for (bitnum = 0; bitnum < ITEM_WIDTH; bitnum++) begin
			bit_sum[bitnum] <= 0;
		end
	end else begin
		for (bitnum = 0; bitnum < ITEM_WIDTH; bitnum++) begin
			case (state)
				s_busy: begin
					item_num <= item_num + 1;
					if (item_num == ITEM_COUNT) begin
						gamma[bitnum] = bit_sum[bitnum] > (ITEM_COUNT / 2);
						answer_o <= gamma * ITEM_WIDTH'(~gamma);
						answer_ready_o <= 1;
						state <= s_done;
					end else begin
						bit_sum[bitnum] <= bit_sum[bitnum] + data_array[item_num][bitnum];
					end
				end
				s_done: begin
					;
				end
			endcase
		end
	end
end

`ifdef COCOTB_SIM
initial begin
	$dumpfile("day3.vcd");
	$dumpvars(0, day3);
end
`endif
endmodule
