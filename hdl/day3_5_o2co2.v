module day3_5_o2co2 #(
	parameter ITEM_COUNT = 12,
	parameter ITEM_WIDTH = 5,
	// 0: calculate oxygen, 1: calculate CO2
	parameter O2_N_CO2 = 0
)(
	input clk_i,
	input rst_i,
	output reg [31:0] answer_o,
	output reg answer_ready_o
);

reg [ITEM_WIDTH-1:0]candidates[ITEM_COUNT-1:0];
reg [$clog2(ITEM_WIDTH)-1:0] candidate_count;

reg [$clog2(ITEM_COUNT)-1:0]item_count;
reg [$clog2(ITEM_COUNT)-1:0]item_count_filtered;
reg [$clog2(ITEM_COUNT)-1:0]item_num;

reg [$clog2(ITEM_COUNT)-1:0]bit_sum;
reg [$clog2(ITEM_WIDTH)-1:0]bit_num;

reg needle;

/* states:
 * s_sum: make a pass through the list of candidates and count the most popular
           bit_num'th bit, then move to s_filter
 * s_filter: make a pass through the list of candidates and add each one with
             the most popular bit determined in s_sum to the list of next
             candidates. Once complete, advance bit_num and return to s_sum
             unless there is only 1 candidate remaining in which case move to
             s_done.
 * s_done: only 1 candidate remains, so latch it out as the answer
 * s_error: more than 1 candidate remains and advancing bit_num would put it
            outside the allowable range
*/
parameter s_sum = 0, s_filter = 1, s_done = 2, s_error = 3;
reg [1:0]state;


initial begin
	$readmemb("../input/day3.bin", candidates);
end

always @(posedge clk_i) begin
	if (rst_i == 1'b1) begin
		item_num <= 0;
		state <= s_sum;
		answer_o <= 0;
		answer_ready_o <= 0;
		item_count <= ITEM_COUNT;
		bit_num <= ITEM_WIDTH-1;
		bit_sum <= 0;
	end else begin
		case (state)
			s_sum: begin
				item_num <= item_num + 1;
				if (item_num == item_count) begin
					needle <= O2_N_CO2 ^ (item_count - bit_sum <= bit_sum);
					item_count_filtered <= 0;
					item_num <= 0;
					state <= s_filter;
				end else begin
					bit_sum <= bit_sum + candidates[item_num][bit_num];
				end
			end
			s_filter: begin
				item_num <= item_num + 1;
				if (item_num < item_count) begin
					if (candidates[item_num][bit_num] == needle) begin
						// match - clobber into next position in candidates
						candidates[item_count_filtered] <= candidates[item_num];
						item_count_filtered <= item_count_filtered + 1;
					end
				end else begin
					// only one left, it's the answer
					if (item_count_filtered == 1) begin
						answer_o <= candidates[0];
						answer_ready_o <= 1;
						state <= s_done;
					end else begin
						if (bit_num == 0) begin
							// no bits left to filter and not finished, error
							state <= s_error;
						end else begin
							// more bits to filter
							item_count <= item_count_filtered;
							bit_num <= bit_num - 1;
							item_num <= 0;
							bit_sum <= 0;
							state <= s_sum;
						end
					end
				end
			end
			s_done: begin
				;
			end
			s_error: begin
				;
			end
		endcase
	end
end

endmodule
