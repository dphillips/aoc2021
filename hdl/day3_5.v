module day3_5 #(
	parameter ITEM_COUNT = 12,
	parameter ITEM_WIDTH = 5
)(
	input clk_i,
	input rst_i,
	output [31:0] answer_o,
	output answer_ready_o
);

wire [31:0]o2;
wire o2_ready;
wire [31:0]co2;
wire co2_ready;

day3_5_o2co2 #(
	.ITEM_COUNT(ITEM_COUNT),
	.ITEM_WIDTH(ITEM_WIDTH),
	.O2_N_CO2(0)
) filter_o2 (
	.clk_i(clk_i),
	.rst_i(rst_i),
	.answer_o(o2),
	.answer_ready_o(o2_ready)
);

day3_5_o2co2 #(
	.ITEM_COUNT(ITEM_COUNT),
	.ITEM_WIDTH(ITEM_WIDTH),
	.O2_N_CO2(1)
) filter_co2 (
	.clk_i(clk_i),
	.rst_i(rst_i),
	.answer_o(co2),
	.answer_ready_o(co2_ready)
);

assign answer_o = co2 * o2;
assign answer_ready_o = co2_ready & o2_ready;

`ifdef COCOTB_SIM
initial begin
$dumpfile("day3_5.vcd");
$dumpvars(0, day3_5);
end
`endif

endmodule

