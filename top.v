module top(
	input clk_25_i
);

wire rst;

reset_gen #(
	.RESET_DURATION_CLOCKS(3)
) reset_gen_inst (
	.clk_i(clk_25_i),
	.rst_o(rst)
);

day1 day1_inst (
	.clk_i(clk_25_i),
	.rst_i(rst)
	//.answer_ready_o(answer_ready_o)
	//.answer_o(answer_o)
);

//pll pll_inst(
//	.clki(clk_25_i),
//	.clko(clk_125)
//);

endmodule
